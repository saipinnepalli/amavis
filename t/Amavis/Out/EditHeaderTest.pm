# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::Out::EditHeaderTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Out::EditHeader' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class, qw(hdr);
}

sub constructor : Tests(3) {
  my $test  = shift;
  my $class = $test->class;
  can_ok $class, 'new';
  ok my $lookup = $class->new,
  '... and the constructor should succeed';
  isa_ok $lookup, $class, '... and the object it returns';
}

sub unfold_hdr {
  # strip eol nl as well
  hdr(@_) =~ s/\n(?=[ \t]|$)//gsr;
}

sub folding : Tests(8) {
  my $test  = shift;

  for my $wsp (' ', "\t") {

    my $raw = "0000${wsp}00000" x 10;

    is
      unfold_hdr(Subject => $raw, 0, undef, 0),
      'Subject: ' . $raw,
      'structured == 0: hdr is reversible';

    is
      unfold_hdr(Subject => $raw, 1, undef, 0),
      'Subject: ' . $raw,
      'structured == 1: hdr does not fold';

    is
      unfold_hdr(Subject => $raw, 2, undef, 0),
      'Subject: ' . $raw,
      'structured == 2: hdr does not fold';
  }

  unlike
    unfold_hdr(Subject => "0000\n00000" x 10, 1, undef, 0),
    qr/\n(?![\t ]|$)/,
    'structured == 1: hdr does not return multiple lines that could be mistaken for multiple headers';

  unlike
    unfold_hdr(Subject => "0000\n00000" x 10, 2, undef, 0),
    qr/\n(?![\t ]|$)/,
    'structured == 2: hdr does not return multiple lines that could be mistaken for multiple headers';
}

1;
